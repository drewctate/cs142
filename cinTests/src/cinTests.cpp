//============================================================================
// Name        : cinTests.cpp
// Author      : Andrew
// Version     :
// Copyright   : GNU Public License
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int i;
	int operation = 1;
	double d;
	cout << "Test cin" << endl; // prints cin
	while (operation > 0) {
		if (cin >> i) {
			cout << cin.fail();
			switch (i)
			{
			case 1:
				cout << "Case 1" << endl;
				break;
			case 2:
				cout << "Case 2" << endl;
				break;
			case 3:
				cout << "Case 3" << endl;
				break;
			default:
				cout << "Case default" << endl;
				break;
			}
		} else {
			cout << cin.fail();
			cin.clear();
			cin.ignore(1000, '\n');
		}
	}
	return 0;
}
