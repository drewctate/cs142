//============================================================================
// Name        : 2pizza.cpp
// Author      : Andrew
// Copyright   : GNU Public License
//============================================================================
/*Tests
 *1: guests = 56
 *	8 large pizzas (no medium or small)
 *	area = 8(3.14159)(10)^2 = 2513.27 inches squared
 *	tip = 23 percent
 *	total = 14.68(8) = 117.44 dollars
 *	total = total + 117.44(.23) = 144.45 dollars (rounded to 144)
 *
 *2: guests = 26
 *	3 large pizzas, 1 medium, 2 small
 *	area = 3(3.14159)(10)^2 + (3.14159)(8)^2 + 2(3.14159)(6)^2 = 1369.73 inches squared
 *	tip = 14 percent
 *	total = 14.68(3) + 11.48(1) + 7.28(2) = 70.08 dollars
 *	total = total + 70.08(.14) = 79.89 dollars (rounded to 80)
 *
 *3: guests = 5
 *	1 medium pizza, 2 small
 *	area = (3.14159)(8)^2 + 2(3.14159)(6)^2 = 427.26 inches squared
 *	tip = 8 percent
 *	total = 11.48 + 7.28(2) = 26.04
 *	total = total + 26.04(.08) = 28.12 (rounded to 28)
 */

#include <iostream>
#include <cmath>
#include <cstring>
using namespace std;

int main() {
	// Define constants
	const double PI = 3.14159;
	const double RADIUS_LARGE = 20.0 / 2;
	const double RADIUS_MEDIUM = 16.0 / 2;
	const double RADIUS_SMALL = 12.0 / 2;
	const double PRICE_LARGE = 14.68;
	const double PRICE_MEDIUM = 11.48;
	const double PRICE_SMALL = 7.28;

	// Get # of guests from user
	cout << "Enter the number of guests: ";
	int guests;
	cin >> guests;

	// Count pizzas
	int largePizzas = guests / 7;
	int mediumPizzas = (guests % 7) / 3;
	int smallPizzas = (guests % 7) % 3;

	// Compute serving size
	double area;
	area = largePizzas * (PI * pow(RADIUS_LARGE, 2));
	area += mediumPizzas * (PI * pow(RADIUS_MEDIUM, 2));
	area += smallPizzas * (PI * pow(RADIUS_SMALL, 2));
	cout << "The total area of pizza will be " << area << " inches squared.\n";

	// Get tip percentage from user
	cout << "Enter the percentage of the tip (0-100): ";
	int tip;
	cin >> tip;
	double tipDecimal = static_cast<double>(tip) / 100.0;

	// Compute total cost
	double total = PRICE_LARGE * largePizzas
			+ PRICE_MEDIUM * mediumPizzas
			+ PRICE_SMALL * smallPizzas;
	total = total + tipDecimal * total; // add tip

	// Round to nearest dollar
	int roundedTotal = total + .5;

	// Output total cost
	cout << "The total cost of the pizza will be $" << roundedTotal << ".";

	return 0;
}
