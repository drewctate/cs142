/*
 * Residential.h
 *
 *  Created on: Dec 2, 2015
 *      Author: drewctate
 */

#ifndef RESIDENTIAL_H_
#define RESIDENTIAL_H_

#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include "Property.h"

class Residential: public Property {
protected:
	bool occupied;

public:
	//--------------------------------------------------------------------------------//
	/*
	 * Constructor
	 *
	 * rental_in
	 * 		Whether the property is rented or not
	 *
	 * value_in
	 * 		Estimated value of property
	 *
	 * address_in
	 * 		Address of property
	 *
	 * occupied_in
	 * 		Whether or not the residential property is occupied
	 */
	Residential(bool rental_in, double value_in, string address_in, bool occupied_in);
	virtual ~Residential();
	//-------------------------------------------------------------------------------//
	/*
	 * toString
	 *
	 */
	const virtual string toString();
	//-------------------------------------------------------------------------------//
	/*
	 * computeTax - computes the tax
	 *
	 */
	virtual void computeTax();
	//-------------------------------------------------------------------------------//
	/*
	 * getTaxReport - returns a report of the taxes
	 *
	 */
	const virtual string getTaxReport ();
};


#endif /* RESIDENTIAL_H_ */
