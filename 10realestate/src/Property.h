/*
 * Property.h
 *
 *  Created on: Nov 30, 2015
 *      Author: drewctate
 */

#ifndef PROPERTY_H_
#define PROPERTY_H_

#pragma once
#include <iostream>

using namespace std;

class Property {
	protected:
		static int idLast;
		bool rental;
		double value;
		double tax;
		int id;
		string address;

	public:
		//--------------------------------------------------------------------------------//
		/*
		 * Constructor
		 *
		 * rental_in
		 * 		Whether the property is rented or not
		 *
		 * value_in
		 * 		Estimated value of property
		 *
		 * address_in
		 * 		Address of property
		 */
		Property (bool rental_in, double value_in, string address_in);
		virtual ~Property();
		const virtual double getTax ();
		const virtual string getAddress ();
		//-------------------------------------------------------------------------------//
		/*
		 * printReport
		 *

		const virtual void printTaxReport();*/
		//-------------------------------------------------------------------------------//
		/*
		 * toString
		 *
		 */
		const virtual string toString () = 0;
		//-------------------------------------------------------------------------------//
		/*
		 * computeTax
		 *
		 */
		virtual void computeTax () = 0;
		/*
		 * computeTax
		 *
		 */
		const virtual string getTaxReport () = 0;
};
#endif /* COMMERCIAL_H_ */
