/*
 * Commercial.h
 *
 *  Created on: Nov 30, 2015
 *      Author: drewctate
 */

#ifndef COMMERCIAL_H_
#define COMMERCIAL_H_

#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include "Property.h"

class Commercial: public Property {
protected:
	double discountRate;
	bool taxDiscount;

public:
	//--------------------------------------------------------------------------------//
	/*
	 * Constructor
	 *
	 * rental_in
	 * 		Whether the property is rented or not
	 *
	 * value_in
	 * 		Estimated value of property
	 *
	 * address_in
	 * 		Address of property
	 *
	 * taxDiscount_in
	 * 		Tax discount
	 *
	 * discountRate_in
	 * 		Discount rate
	 */
	Commercial (bool rental_in, double value_in, string address_in, bool taxDiscount_in, double discountRate_in);
	virtual ~Commercial();
	//-------------------------------------------------------------------------------//
	/*
	 * toString
	 *
	 */
	const virtual string toString ();
	//-------------------------------------------------------------------------------//
	/*
	 * computeTax - computes the tax
	 *
	 */
	virtual void computeTax ();
	//-------------------------------------------------------------------------------//
	/*
	 * getTaxReport - returns a report of the taxes
	 *
	 */
	const virtual string getTaxReport ();
};



#endif /* COMMERCIAL_H_ */
