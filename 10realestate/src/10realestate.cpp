/*
 * 10realestate.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: drewctate
 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <sstream>
#include "Commercial.h"
#include "Residential.h"

using namespace std;

void print (vector<string> sentence) {
	for (int i = 0; i < sentence.size(); i++) {
		cout << sentence[i] << ' ';
	}
	cout << endl;
}

void error (vector<string> line, string type) {
	cout << "Ignoring bad " << type << " in input file: ";
	print(line);
}

void addProperty (vector<string> line, vector<Property*> &properties) {
	double value;
	double discountRate;
	bool taxDiscount;
	bool occupied;
	bool rental;
	string address;
	string str;

	Commercial* com = NULL;
	Residential* res = NULL;


	if (line[0] == "Commercial") {
		str = line[1];
		if ((str != "0") && (str != "1")) {
			error(line, "COMMERCIAL");
		}
		else {
			try {
				rental = atoi(str.c_str());

				str = line[2];
					value = stod(str.c_str());
					str = line[3];
					if ((str != "0") && (str != "1")) {
						error(line, "COMMERCIAL");
					}
					else {
						taxDiscount = atoi(str.c_str());
						str = line[4];
						discountRate = stod(str.c_str());

						str = "";
						for (int i = 5; i < line.size(); i++) {
							str = str + line[i] + " ";
						}
						address = str;

						com = new Commercial(rental, value, address, taxDiscount, discountRate);
						properties.push_back(com);
				}
			}
			catch (exception& e) {
				error(line, "COMMERCIAL");
			}
		}
	}
	else if (line[0] == "Residential") {
		str = line[1];
		if ((str != "0") && (str != "1")) {
			error(line, "RESIDENTIAL");
		}
		else {
			try {
				rental = atoi(str.c_str());

				str = line[2];
					value = stod(str.c_str());
					str = line[3];
					if ((str != "0") && (str != "1")) {
						error(line, "RESIDENTIAL");
					}
					else {
						occupied = atoi(str.c_str());
						str = "";

						for (int i = 4; i < line.size(); i++) {
							str = str + line[i] + " ";
						}
						address = str;

						res = new Residential(rental, value, address, occupied);
						properties.push_back(res);
				}
			}
			catch (exception& e) {
				error(line, "RESIDENTIAL");
			}
		}
	}
	else {
		cout << "Ignoring unknown types of properties appearing in the input file: ";
		print(line);
	}
}

// Generates vector of strings from one string with ' ' as delimiter
vector<string> split (string str) {
	vector<string> splitString;
	string substr;
	for (int i = 0; i < str.length(); i++) {
		  if (i == str.length() - 1) {
			  substr += str[i];
			  splitString.push_back(substr);
			  substr = "";
		  }
		  else if (str[i] != ' ') {
			  substr += str[i];
	 	  }
		  else {
			  splitString.push_back(substr);
			  substr = "";
		  }
	  }
	return splitString;
}

int main () {
	string fname;
	string line;
	string option;
	Property* temp = NULL;
	vector<Property*> properties;
	vector<string> propertyInfo;

	cout << "Enter name of file to load: ";
	cin >> fname;

	ifstream ifs(fname, ifstream::in);

	if (ifs.is_open()) {
		while(getline(ifs, line)) {
			propertyInfo = split(line);
			addProperty(propertyInfo, properties);
		}
		cout << "\nAll valid properties: \n\n";

		for (int i = 0; i < properties.size(); i++) {
			cout << properties[i]->toString() << endl;
		}

		string option;
		cout << "\nEnter \"address\" to print tax reports in order of address or \"taxes\" to print tax reports in order of taxes: ";
		cin >> option;

		cout << "\nNOW PRINTING TAX REPORT:\n\n";
		if (option == "address") {
			cout << "hahahahaha";

			int jplus1 = 0;
			for (int i = 0; i < properties.size(); i++) {
				for (int j = 0; j < properties.size(); j++) {
					jplus1 = j + 1;
					cout << properties[jplus1]->getAddress();
					/*if ((properties[i]->getAddress()) > (properties[i + 1]->getAddress())) {
						temp = properties[i];
						properties[i] = properties[i + 1];
						properties[i + 1] = temp;
					}*/
				}
			}
		}
		else if (option == "taxes") {
			cout << "Sorting by Taxes";
			for (int i = 0; i < properties.size(); i++) {
				for (int j = 0; j < properties.size(); j++) {
					if (properties[i]->getTax() > properties[i + 1]->getTax()) {
						cout << properties[i]->getTax();
						temp = properties[i];
						properties[i] = properties[i + 1];
						properties[i + 1] = temp;
					}
				}
			}
		}
		else {
			cout << "Invalid input.";
		}

		for (int i = 0; i < properties.size(); i++) {
			cout << properties[i]->getTaxReport() << endl;
		}

		ifs.close();
	}
	else {
		cout << "Unable to open file.\n";
	}


	return 0;
}
