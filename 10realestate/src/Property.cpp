/*
 * Property.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: drewctate
 */
#include "Property.h"
using namespace std;

int Property::idLast = 0;

Property::Property (bool rental_in, double value_in, string address_in) {
	this->rental = rental_in;
	this->value = value_in;
	this->address = address_in;
	this->id = idLast;
	idLast++;
}

Property::~Property (){}

const double Property::getTax () {
	return tax;
}

const string Property::getAddress () {
	return address;
}
