/*
 * Commercial.cpp
 *
 *  Created on: Nov 30, 2015
 *      Author: drewctate
 */

#include "Commercial.h"

Commercial::Commercial(bool rental_in, double value_in, string address_in, bool taxDiscount_in, double discountRate_in):Property(rental_in, value_in, address_in) {
	this->discountRate = discountRate_in;
	this->taxDiscount = taxDiscount_in;

	this->computeTax();
}

Commercial::~Commercial(){}

const string Commercial::toString() {
	stringstream ss;
	ss << "Property ID: " << id
			<< " Address: " << address;

	if (rental)
		ss << " RENTAL ";
	else
		ss << " NOT RENTAL ";

	ss << " Estimated value: " << value;

	if (taxDiscount)
		ss << " Discounted  Discount: " << discountRate;
	else
		ss << " Not discounted";

	return ss.str();
}

void Commercial::computeTax() {
	const double rentalRate = 0.012;
	const double nonRentalRate = .01;
	if (rental)
		if (taxDiscount) {
			tax = (1 - discountRate) * value * rentalRate;
		}
		else {
			tax = value * rentalRate;
		}
	else
		if (taxDiscount) {
			tax = (1 - discountRate) * value * nonRentalRate;
		}
		else {
			tax = value * nonRentalRate;
		}
}

const string Commercial::getTaxReport() {
	stringstream ss;
	ss << "** Taxes due for the property at: " << address << endl
			<< "\tProperty ID\t\t\t\t\t" << id << endl
			<< "\tThis property has an estimated value of:\t" << value << endl
			<< "\tTaxes due on this property are:\t\t\t" << tax;
	return ss.str();
}
