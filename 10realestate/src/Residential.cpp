/*
 * Residential.cpp
 *
 *  Created on: Dec 2, 2015
 *      Author: drewctate
 */

#include "Residential.h"

Residential::Residential (bool rental_in, double value_in, string address_in, bool occupied_in):Property(rental_in, value_in, address_in) {
	this->occupied = occupied_in;
	this->computeTax();
}

Residential::~Residential () {}

const string Residential::toString () {
	stringstream ss;
	ss << "Property ID: " << id
			<< " Address: " << address;

	if (rental)
		ss << " RENTAL ";
	else
		ss << " NOT RENTAL ";

	ss << " Estimated value: " << value;

	if (occupied)
		ss << " Occupied";
	else
		ss << " Not occupied";

	return ss.str();
}

const string Residential::getTaxReport () {
	stringstream ss;
	ss << "** Taxes due for the property at: " << address << endl
			<< "\tProperty ID\t\t\t\t\t" << id << endl
			<< "\tThis property has an estimated value of:\t" << value << endl
			<< "\tTaxes due on this property are:\t\t\t" << tax;
	return ss.str();
}

void Residential::computeTax () {
	const double occupiedRate = .006;
	const double nonOccupiedRate = .009;
	if (occupied)
		tax = value * occupiedRate;
	else
		tax = value * nonOccupiedRate;
}
