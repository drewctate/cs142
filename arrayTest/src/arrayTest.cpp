#include <iostream>

using namespace std;
//
void addValue(int arr[][2], int val) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            arr[i][j] += val;
        }
    }
}

void print(int arr[][2]) {
    for (int i = 0; i < 2; i++) {
        cout << arr[i][0] << " " << arr[i][1] << endl;
    }
}

int main()
{
     int my_array[2][2] = {{5,4},{23,-1}};

     int value;
     cin >> value;

     addValue(my_array, value);

     print(my_array);

     return 0;
}
