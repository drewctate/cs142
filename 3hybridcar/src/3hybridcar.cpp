//============================================================================
// Name        : 3hybridcar.cpp
// Author      : Andrew
// Version     :
// Copyright   : GNU Public License
//============================================================================

/*
Test 1:
 * 200 miles per year
 * 2.7 price
 * 300000 initial cost of hybrid
 * 100 efficiency of hybrid car
 * 100 miles per gal hybrid
 * 200000 resale hybrid
 *
 * 200000 non-hybrid car
 * 50 miles per gallon
 * 100000 resale
 * Gas
 *
Hybrid Car:
Total gallons consumed over 5 year period: 10
Total cost of owning car over 5 year period: 100027
Non-Hybrid Car:
Total gallons consumed over 5 year period: 20
Total cost of owning car over 5 year period: 100054

Test 2:
Enter estimated miles driven per year: 6000
Enter estimated price of a gallon of gas during the 5 years of ownership: 3
Enter initial cost of a hybrid car: 3000
Enter efficiency of the hybrid car in miles per gallon: 80
Enter estimated resale value (a dollar amount) for a hybrid after 5 years: 2000
Enter initial cost of a non-hybrid car: 2000
Enter efficiency of the non-hybrid car in miles per gallon: 30
Enter estimated resale value (a dollar amount) for a non-hybrid after 5 years: 1000
Shall the program evaluate based on Gas consumption or Total cost? Total
Hybrid Car:
Total gallons consumed over 5 year period: 375
Total cost of owning car over 5 year period: 2125
Non-Hybrid Car:
Total gallons consumed over 5 year period: 1000
Total cost of owning car over 5 year period: 4000

Test 3:
Enter estimated miles driven per year: 30
Enter estimated price of a gallon of gas during the 5 years of ownership: 5
Enter initial cost of a hybrid car: 4000
Enter efficiency of the hybrid car in miles per gallon: 29
Enter estimated resale value (a dollar amount) for a hybrid after 5 years: 2000
Enter initial cost of a non-hybrid car: 5000
Enter efficiency of the non-hybrid car in miles per gallon: 69
Enter estimated resale value (a dollar amount) for a non-hybrid after 5 years: 6000
Shall the program evaluate based on Gas consumption or Total cost? Gas
Non-Hybrid Car:
Total gallons consumed over 5 year period: 2.17391
Total cost of owning car over 5 year period: -989.13
Hybrid Car:
Total gallons consumed over 5 year period: 5.17241
Total cost of owning car over 5 year period: 2025.86
 *
 */

#include <iostream>
using namespace std;

int main() {
	double milesPerYear;
	double gallonPrice;
	double initialHybridCost;
	double efficiencyHybrid;
	double resaleHybrid;
	double initialNonHCost;
	double efficiencyNonH;
	double resaleNonH;
	string criterion;

	double totalHybridGallons;
	double totalHybridCost;
	double totalNonHGallons;
	double totalNonHCost;
	double numberYears = 5;

	// Get inputs
	cout << "Enter estimated miles driven per year: ";
	cin >> milesPerYear;
	if (!(milesPerYear > 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> milesPerYear;
	}

	cout << "Enter estimated price of a gallon of gas during the 5 years of ownership: ";
	cin >> gallonPrice;
	if (!(gallonPrice > 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> gallonPrice;
	}

	cout << "Enter initial cost of a hybrid car: ";
	cin >> initialHybridCost;
	if (!(initialHybridCost > 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> initialHybridCost;
	}

	cout <<  "Enter efficiency of the hybrid car in miles per gallon: ";
	cin >> efficiencyHybrid;
	if (!(efficiencyHybrid > 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> efficiencyHybrid;
	}

	cout <<  "Enter estimated resale value (a dollar amount) for a hybrid after 5 years: ";
	cin >> resaleHybrid;
	if (!(resaleHybrid > 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> resaleHybrid;
	}

	cout <<  "Enter initial cost of a non-hybrid car: ";
	cin >> initialNonHCost;
	if (!(initialNonHCost> 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> initialNonHCost;
	}

	cout <<  "Enter efficiency of the non-hybrid car in miles per gallon: ";
	cin >> efficiencyNonH;
	if (!(efficiencyNonH > 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> efficiencyNonH;
	}

	cout <<  "Enter estimated resale value (a dollar amount) for a non-hybrid after 5 years: ";
	cin >> resaleNonH;
	if (!(resaleNonH > 0))
	{
		cout << "Only positive numbers are valid: ";
		cin >> resaleNonH;
	}

	cout <<  "Shall the program evaluate based on Gas consumption or Total cost? ";
	cin >> criterion;

	// Calculations
	totalHybridGallons = (milesPerYear * numberYears) / efficiencyHybrid;
	totalHybridCost = totalHybridGallons * gallonPrice + (initialHybridCost - resaleHybrid);

	totalNonHGallons = (milesPerYear * numberYears) / efficiencyNonH;
	totalNonHCost = totalNonHGallons * gallonPrice + (initialNonHCost - resaleNonH);

	// Output hybrid costs
	if (criterion == "Gas")
	{
		if (totalHybridGallons < totalNonHGallons)
		{
			cout << "Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalHybridGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalHybridCost << endl;

			cout << "Non-Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalNonHGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalNonHCost << endl;
		}
		else
		{
			cout << "Non-Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalNonHGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalNonHCost << endl;

			cout << "Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalHybridGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalHybridCost << endl;
		}
	}
	else
	{
		if (totalHybridCost < totalNonHCost)
		{
			cout << "Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalHybridGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalHybridCost << endl;

			cout << "Non-Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalNonHGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalNonHCost << endl;
		}
		else
		{
			cout << "Non-Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalNonHGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalNonHCost << endl;

			cout << "Hybrid Car:" << endl;
			cout << "Total gallons consumed over 5 year period: " << totalHybridGallons << endl;
			cout << "Total cost of owning car over 5 year period: " << totalHybridCost << endl;
		}
	}
	return 0;
}
