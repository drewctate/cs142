/*
Match 1/3, Round 1/4 --- Choose 1: Nova or 2: Communal: 2
Match 1/3, Round 2/4 --- Choose 1: Bombay House or 2: J-dawgs: 2
Match 1/3, Round 3/4 --- Choose 1: Station 22 or 2: SLAB Pizza: 2
Match 1/3, Round 4/4 --- Choose 1: Spark or 2: Black Sheep Cafe: 2
Match 2/3, Round 1/2 --- Choose 1: Communal or 2: J-dawgs: 2
Match 2/3, Round 2/2 --- Choose 1: SLAB Pizza or 2: Black Sheep Cafe: 2
Match 3/3, Round 1/1 --- Choose 1: J-dawgs or 2: Black Sheep Cafe: 1
J-dawgs is the winner!

Match 1/3, Round 1/4 --- Choose 1: Nova or 2: SLAB Pizza: 2
Match 1/3, Round 2/4 --- Choose 1: Bombay House or 2: J-dawgs: 1
Match 1/3, Round 3/4 --- Choose 1: Spark or 2: Communal: 2
Match 1/3, Round 4/4 --- Choose 1: Station 22 or 2: Black Sheep Cafe: 2
Match 2/3, Round 1/2 --- Choose 1: SLAB Pizza or 2: Bombay House: 1
Match 2/3, Round 2/2 --- Choose 1: Communal or 2: Black Sheep Cafe: 2
Match 3/3, Round 1/1 --- Choose 1: SLAB Pizza or 2: Black Sheep Cafe: 1

Match 1/3, Round 1/4 --- Choose 1: Nova or 2: SLAB Pizza: 1
Match 1/3, Round 2/4 --- Choose 1: Bombay House or 2: J-dawgs: 2
Match 1/3, Round 3/4 --- Choose 1: Spark or 2: Communal: 1
Match 1/3, Round 4/4 --- Choose 1: Station 22 or 2: Black Sheep Cafe: 1
Match 2/3, Round 1/2 --- Choose 1: Nova or 2: J-dawgs: 1
Match 2/3, Round 2/2 --- Choose 1: Spark or 2: Station 22: 1
Match 3/3, Round 1/1 --- Choose 1: Nova or 2: Spark: 2
Spark is the winner!
 */
#include <vector>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <stdlib.h>
#include <time.h>

using namespace std;

void printVector(vector<string> rs) {
	for (int i=0; i<rs.size(); i++) {
		if (i != rs.size()-1)
			cout << rs[i] << ", ";
		else cout << rs[i];
	}
	cout << "\n\n";
}

int find(string res, vector<string> rs)
{
	int location = -1;
	for (int i=0; i<rs.size(); i++) {
		if (rs[i] == res)
			location = i;
	}
	return location;
}

void addRes(string res, vector<string>& rs) {
	if (find(res, rs) == -1) {
		rs.push_back(res);
		cout << res << " added to list of restaurants.\n\n";
	} else
		cout << res << " is already in the list.\n\n";
}

void removeRes(string res, vector<string>& rs) {
	int pos = find(res, rs);
	if (pos == -1) {
		cout << res << " isn't in the list.\n\n";
	} else {
		rs.erase(rs.begin() + pos);
		cout << res << " removed from list of restaurants.\n\n";
	}
}

void shuffle(vector<string>& rs) {
	int rand1;
	int rand2;
	string temp;
	for (int i = 0; i < rs.size() * 2; i++) {
		rand1 = rand() % rs.size();
		rand2 = rand() % rs.size();
		temp = rs[rand1];
		rs[rand1] = rs[rand2];
		rs[rand2] = temp;
	}
}

int battle(vector<string> rs) {
	int choice;
	int pos;
	vector<string> mark;
	double logn = (log(rs.size()) / log(2));
	int check = (log(rs.size()) / log(2));
	if ((logn - check) == 0) {

		for (int i = 0; i < logn; i++) {
			int size = rs.size();
			for (int r = 0; r < size / 2; r++) {
				cout << "Match " << i + 1 << "/" << logn << ", Round " << r + 1 << "/" << size / 2 <<
						" --- Choose 1: " << rs[r] << " or 2: " << rs[r + 1] << ": ";
				if (cin >> choice) {
					if (choice == 1) {
						pos = find(rs[r+1], rs);
						rs.erase(rs.begin() + pos);
					} else if (choice == 2) {
						pos = find(rs[r], rs);
						rs.erase(rs.begin() + pos);
					} else {
						cout << "Invalid option.\n";
						r -= 1;
					}
				} else {
					cout << "Invalid input.\n";
					cin.clear();
					cin.ignore(1000, '\n');
					r -= 1;
				}
			}
		}
		cout << rs[0] << " is the winner! \n\n";
		return -1;
	} else {
		cout << "The list of restaurants must be equal to 2^n \n\n";
		return 0;
	}
}

int main()
{
	int i = 0;
	string res;
	vector<string> rs;
	rs.push_back("Nova");
	rs.push_back("SLAB Pizza");
	rs.push_back("Bombay House");
	rs.push_back("J-dawgs");
	rs.push_back("Spark");
	rs.push_back("Communal");
	rs.push_back("Station 22");
	rs.push_back("Black Sheep Cafe");

	srand(time(0));

	while(i != -1) {
		cout << "Select one of the option\n"
		<< "1 - Display all restaurants\n"
		<< "2 - Add a restaurant\n"
		<< "3 - Remove a restaurant\n"
		<< "4 - Shuffle the vector\n"
		<< "5 - Begin the tournament\n"
		<< "6 - Quit the program\n\n"
		<< "Choose: ";

		if (!(cin >> i)) {
			i = 100;
			cin.clear();
			cin.ignore(1000, '\n');
		}

		switch(i) {
			case 1:
				printVector(rs);
				break;

			case 2:
				cout << "Enter restaurant name: ";
				cin.ignore(1000, '\n');
				getline(cin,res);
				addRes(res, rs);
				break;

			case 3:
				cout << "Enter restaurant name: ";
				cin.ignore(1000, '\n');
				getline(cin,res);
				removeRes(res, rs);
				break;

			case 4:
				shuffle(rs);
				cout << "List shuffled.\n\n";
				break;

			case 5:
				i = battle(rs);
				break;

			case 6:
				i = -1;
				break;

			default:
				break;
		}
	}
	return 0;
}
