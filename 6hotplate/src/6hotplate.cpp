/*
 * 6hotplate.cpp
 *
 *  Created on: Oct 27, 2015
 *      Author: drewctate
 */
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

const int plateWidth = 20;
const int plateHeight = 20;
const int edgeTemp = 100;

void initPlate(double hot_plate[][plateWidth]) {
	for (int i = 0; i < plateHeight; i++) {
		for (int j = 0; j < plateWidth; j++) {
			hot_plate[i][j] = 0;
		}
	}

	for (int i = 0; i < plateWidth; i++) {
		hot_plate[0][i] = edgeTemp;
		hot_plate[plateHeight - 1][i] = edgeTemp;
	}

	hot_plate[0][0] = 0;
	hot_plate[plateHeight - 1][0] = 0;
	hot_plate[0][plateWidth - 1] = 0;
	hot_plate[plateHeight - 1][plateWidth - 1] = 0;
}

void print(double hot_plate[][plateWidth]) {
	for (int i = 0; i < plateHeight; i++) {
		for (int j = 0; j < plateWidth; j++) {
			cout << setw(7) << fixed << setprecision(1) << hot_plate[i][j];
		}
		cout << endl;
	}
	cout << endl;
}

void copyArray(double oldArr[][plateWidth], double newArr[][plateWidth]) {
	for (int i = 0; i < plateHeight; i++) {
		for (int j = 0; j < plateWidth; j++) {
			newArr[i][j] = oldArr[i][j];
		}
	}
}

void update(double hot_plate[][plateWidth], double& maxChange) {
	double hot_plate_old[plateHeight][plateWidth];
	copyArray(hot_plate, hot_plate_old);
	double avg = 0;
	for (int i = 1; i < plateHeight - 1; i++) {
		for (int j = 1; j < plateWidth - 1; j++) {
			avg = (hot_plate_old[i-1][j] + hot_plate_old[i][j+1] + hot_plate_old[i+1][j] + hot_plate_old[i][j-1]) / 4;
			if (abs(hot_plate[i][j] - avg) > maxChange)
				maxChange = abs(hot_plate[i][j] - avg);
			hot_plate[i][j] = avg;
		}
	}
}

void write(double hot_plate[][plateWidth]) {
	ofstream file;
	file.open("lab6output.csv");
	for (int i = 0; i < plateHeight; i++) {
		for (int j = 0; j < plateWidth; j++) {
			file << hot_plate[i][j] << ",";
		}
		file << endl;
	}
	file.close();
}

int main() {
	// initialize hot plate
	double hot_plate[plateHeight][plateWidth];
	initPlate(hot_plate);
	print(hot_plate);

	// heat hot plate until no cell changes more than 0.1
	double maxChange = 0;
	double max = 0.1;
	int i = 0;
	update(hot_plate, maxChange);
	print(hot_plate);
	while (i != -1) {
		update(hot_plate, maxChange);
		if (maxChange <= max)
			i = -1;
		maxChange = 0;
	}

	// print heated hot plate to console and file
	print(hot_plate);
	write(hot_plate);
	return 0;
}
