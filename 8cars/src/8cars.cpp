/*
 * 8cars.cpp
 *
 *  Created on: Nov 17, 2015
 *      Author: Andrew Tate
 *
 *
 *
----------------------------------------------
TEST CASE 1
----------------------------------------------
Select one of the options
1 - Show current inventory
2 - Show current balance
3 - Buy a car
4 - Sell a car
5 - Paint a car
6 - Load File
7 - Save File
8 - Quit the program

Choose: 6
Enter file name: cars2.txt
Select one of the options
1 - Show current inventory
2 - Show current balance
3 - Buy a car
4 - Sell a car
5 - Paint a car
6 - Load File
7 - Save File
8 - Quit the program

Choose: 1
Name: Navy
Color: White
Price: $6972.15
--------------------------------
Name: Kidney
Color: Red
Price: $3971.15
--------------------------------
Name: Refried
Color: Brown
Price: $9999.99
--------------------------------
Name: Garbanzo
Color: White
Price: $975.21
--------------------------------
Name: Black
Color: Black
Price: $7946.85
--------------------------------
Name: Edamame
Color: Green
Price: $555.55
--------------------------------
Name: Lima
Color: White
Price: $5873.15
--------------------------------
Name: Pinto
Color: Brown
Price: $12369
---------------------------------------
TEST CASE 2
---------------------------------------
Select one of the options
1 - Show current inventory
2 - Show current balance
3 - Buy a car
4 - Sell a car
5 - Paint a car
6 - Load File
7 - Save File
8 - Quit the program

Choose: 2
Current balance: 79715.40

---------------------------------------
TEST CASE 3
---------------------------------------
Select one of the options
1 - Show current inventory
2 - Show current balance
3 - Buy a car
4 - Sell a car
5 - Paint a car
6 - Load File
7 - Save File
8 - Quit the program

Choose: 3
	Enter name: Enzo Ferrari
	Enter color: Blue, Baby Blue
	Enter price: 1000000
You don't have enough money for that car!
 *
 *
 *
 */
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include "Car.h"

using namespace std;

// returns the index value of a car with given name or -1 if name not found
int find (string name, vector<Car*> cars) {
	int index = -1;

	for (int i = 0; i < cars.size(); i++) {
		if (cars[i]->getName() == name) {
			index = i;
		}
	}
	return index;
}

// prints a vector of car
void printCars (vector<Car*> cars) {
	for (int i = 0; i < cars.size(); i++) {
		cout << cars[i]->toString()
		<< "--------------------------------\n";
	}
	cout << endl << endl;
}

// loads cars and current balance from file, adding them to working balance and collection
void loadFile (string fname, double& currentBalance, vector<Car*>& cars) {
	string name;
	string color;
	double price;
	double plusBalance;
	Car* newCar = NULL;
	ifstream ifs(fname, ifstream::in);
	string line;

	if (ifs.is_open())
	{
		ifs >> plusBalance;
		currentBalance += plusBalance;
		while ( ifs >> name >> color >> price )
	    {
			newCar = new Car(name, color, price);
			cars.push_back(newCar);
	    }

		ifs.close();
	}
	else {
		cout << "Unable to open file\n";
	}
}

// saves existing cars and balance to given file name
void save (string fname, double currentBalance, vector<Car*> cars) {
	ofstream ofs(fname, ofstream::out);
	string line;

	if (ofs.is_open())
	{
		ofs << currentBalance << endl;
		for (int i = 0; i < cars.size(); i++)
	    {
			ofs << cars[i]->getName() << ' '
			<< cars[i]->getColor() << ' '
			<< cars[i]->getPrice() << endl;
	    }

		ofs.close();
	}
	else {
		cout << "Unable to create file\n";
	}
}

int main() {
	string name, color;
	double price;
	double currentBalance = 10000;
	Car* newCar = NULL;
	int i = 0;
	int index;
	vector<Car*> cars;
	string fname;

	while(i != -1) {
		cout << "Select one of the options\n"
		<< "1 - Show current inventory\n"
		<< "2 - Show current balance\n"
		<< "3 - Buy a car\n"
		<< "4 - Sell a car\n"
		<< "5 - Paint a car\n"
		<< "6 - Load File\n"
		<< "7 - Save File\n"
		<< "8 - Quit the program\n\n"
		<< "Choose: ";

		if (!(cin >> i) || (i == -1)) {
			i = 0;
			cin.clear();
			cin.ignore(1000, '\n');
		}

		switch(i) {

			case 1:
				printCars(cars);
				break;

			case 2:
				cout << "Current balance: "
				<< fixed << setprecision(2) << currentBalance << endl;
				break;

			case 3:
				cout << "\tEnter name: ";
				cin.ignore(1000, '\n');
				getline(cin,name);

				if (!(find(name, cars) == -1)) break;

				cout << "\tEnter color: ";
				getline(cin,color);

				cout << "\tEnter price: ";
				cin >> price;

				if (price <= currentBalance) {
					newCar = new Car(name, color, price);
					cars.push_back(newCar);
					currentBalance -= price;
				}
				else cout << "You don't have enough money for that car!" << endl;
				break;

			case 4:
				cout << "Enter name of car: ";
				cin >> name;
				index = find(name, cars);
				if (index != -1) {
					currentBalance += cars[index]->getPrice();
					delete cars[index];
					cars.erase(cars.begin() + index);
				}
				else {
					cout << "Car doesn't exist.\n";
				}
				break;

			case 5:
				cout << "Enter name of car: ";
				cin >> name;
				index = find(name, cars);
				if (index != -1) {
					cout << "Select new color: ";
					cin >> color;
					cars[index]->paint(color);
				}
				else {
					cout << "Car doesn't exist.\n";
				}
				break;

			case 6:
				cout << "Enter file name: ";
				if (!(cin >> fname)) {
					i = 100;
					cin.clear();
					cin.ignore(1000, '\n');
				}
				else loadFile(fname, currentBalance, cars);
				cout << "File loaded.\n\n";
				break;

			case 7:
				cout << "Enter file name: ";
				if (!(cin >> fname)) {
					i = 100;
					cin.clear();
					cin.ignore(1000, '\n');
				}
				else save(fname, currentBalance, cars);
				break;

			case 8:
				i = -1;
				break;

			default:
				break;
		}
	}
	return 0;
}
