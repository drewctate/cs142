/*
 * Andrew Tate
 * Student ID: 30-267-6449 03
 * CS 142 Fall 2015 Midterm 2 Exam
 *
 * Test Cases
 * 	1. Where have all those good people gone?
 * 	Expected: Hereway avehay allyay osethay oodgay eoplepay onegay? (I made a mistake translating "Where")
 * 	Translated:Erewhay avehay allyay osethay oodgay eoplepay onegay?
 *
 * 	2. The Radish?? It only 'floats" on One side!!
 * 	Expected: Ethay Adishray?? Ityay onlyyay 'oatsflay" onyay Oneyay idesay!!
 * 	Translated: Ethay Adishray?? Ityay onlyyay 'oatsflay" onyay Oneyay idesay!!
 *
 * 	3. Why can't the real world... just stop hassling Me?
 * 	Expected: Whyay an'tcay ethay ealray orldway... ustjay opstay asslinghay Emay?
 * 	Translated: Whyay an'tcay ethay ealray orldway... ustjay opstay asslinghay Emay?
 */

#include <iostream>
#include <string>
#include <vector>
#include <cctype>

using namespace std;
const char space = ' ';

// Generates vector of strings from one string with ' ' as delimiter
vector<string> getSentenceVector (string str) {
	vector<string> sentence;
	string substr;
	for (int i = 0; i < str.length(); i++) {
		  if (i == str.length() - 1) {
			  substr += str[i];
			  sentence.push_back(substr);
			  substr = "";
		  }
		  else if (str[i] != space) {
			  substr += str[i];
	 	  }
		  else {
			  sentence.push_back(substr);
			  substr = "";
		  }
	  }
	return sentence;
}

// Checks if a character is a vowel
bool isVowel (char ch) {
	bool vowel;
	switch (ch) {
		case 'a': vowel = true; break;
		case 'e': vowel = true; break;
		case 'i': vowel = true; break;
		case 'o': vowel = true; break;
		case 'u': vowel = true; break;

		case 'A': vowel = true; break;
		case 'E': vowel = true; break;
		case 'I': vowel = true; break;
		case 'O': vowel = true; break;
		case 'U': vowel = true; break;

		default: vowel = false; break;
	}
	return vowel;
}

// Checks if a character is capital
bool isCapital (char ch) {
	bool capital;
	switch (ch) {
		case 'A': capital = true; break;
		case 'B': capital = true; break;
		case 'C': capital = true; break;
		case 'D': capital = true; break;
		case 'E': capital = true; break;
		case 'F': capital = true; break;
		case 'G': capital = true; break;
		case 'H': capital = true; break;
		case 'I': capital = true; break;
		case 'J': capital = true; break;
		case 'K': capital = true; break;
		case 'L': capital = true; break;
		case 'M': capital = true; break;
		case 'N': capital = true; break;
		case 'O': capital = true; break;
		case 'P': capital = true; break;
		case 'Q': capital = true; break;
		case 'R': capital = true; break;
		case 'S': capital = true; break;
		case 'T': capital = true; break;
		case 'U': capital = true; break;
		case 'V': capital = true; break;
		case 'W': capital = true; break;
		case 'X': capital = true; break;
		case 'Y': capital = true; break;
		case 'Z': capital = true; break;

		default: capital = false; break;
	}
	return capital;
}

// Checks if a character is a punctuation mark
bool isPunc (char ch) {
	bool punc;
	switch (ch) {
		case '#': punc = true; break;
		case '!': punc = true; break;
		case '?': punc = true; break;
		case '.': punc = true; break;
		case ',': punc = true; break;
		case ';': punc = true; break;
		case ':': punc = true; break;
		case '\'': punc = true; break;
		case '"': punc = true; break;

		default: punc = false; break;
	}
	return punc;
}

// cleans and stores leading a trailing punctuation, returning punctuation-cleaned string
string cleanPunc (string str, string& prepunc, string& postpunc) {
	int i = 0;
	int j = 0;

	while (i != -1) { // check for and store preceding punctuation
		if (j == (str.length() - 1)) {
			prepunc = "";
			return str;
		}
		else if (!(isPunc(str[j]))) {
			str = str.substr(j, str.length() - j);
			i = -1;
		}
		else {
			prepunc += str[j];
		}
		j++;
	}

	i = 0;
	j = str.length() - 1;
	while (i != -1) { // check for and store trailing punctuation
		if (!(isPunc(str[j]))) {
			str = str.substr(0, j + 1);
			i = -1;
		}
		else {
			postpunc = str[j] + postpunc;
		}
		j--;
	}
	return str;
}

// translates one string into pig latin
string translateWord (string str) {
	string prepunc;
	string postpunc;
	string firstsyl;
	string rest;
	bool capital;
	int i = 0;
	int j = 0;

	str = cleanPunc(str, prepunc, postpunc); // get rid of and store trailing and preceding punctuation

	if (isCapital(str[0])) { //check for leading capitalization
		capital = true;
		str[0] = tolower(str[0]);
	}

	if (isVowel(str[0])) {
		str += "yay";
	}
	else { // if str doesn't start with a vowel, make the swap
		while (i != -1) {
			if ((j == (str.length() - 1)) && isPunc(str[j]) ) { // catch for punctuation-only words
				return str;
			}
			else if ((j == (str.length() - 1)) && !(isVowel(str[j]))) { // catch for consonant-only words
				if (capital) { // recapitalize
					str[0] = toupper(str[0]);
					capital = false;
				}
				return str + "ay";
			}
			else if (isVowel(str[j])) {
				rest = str.substr(j, str.length() - firstsyl.length());
				i = -1;
			}
			else {
				firstsyl += str[j];
			}
			j++;
		}
		str = rest + firstsyl + "ay";
	}

	if (capital) { // recapitalize
		str[0] = toupper(str[0]);
		capital = false;
	}

	return prepunc + str + postpunc; // reconstruct with punctuation and return
}

// translates a vector of string into pig latin
void translate (vector<string>& sentence) {
	string str;
	for (int i = 0; i < sentence.size(); i++) {
		str = sentence[i];
		sentence[i] = translateWord(str);
	}
}

// prints a vector of strings
void print (vector<string> sentence) {
	for (int i = 0; i < sentence.size(); i++) {
		cout << sentence[i] << space;
	}
	cout << endl << endl;
}

int main ()
{
	vector<string> sentence;
	string str;

	cout << "Enter sentence to translate (or \"opstay\" to stop the program): ";
	getline(cin, str);
	while (str != "opstay") {
		sentence = getSentenceVector(str);
		cout << "Print words: ";
		print(sentence);
		translate(sentence);
		cout << "Here is the translation: ";
		print(sentence);

		cout << "Enter sentence to translate: ";
		getline(cin, str);
	}
	return 0;
}
