//============================================================================
// Name        : HelloWorld.cpp
// Author      : Andrew
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "What was the original name of BYU?\t\t" << "Brigham Young Acadamy\n"
		<< "When was BYA established?\t\t\t" << "1875\n"
		<< "Who was the first \"permanent\" principle of BYU?\t" << "Karl Maeser\n"
		<< "When did BYA become BYU?\t\t\t" << "1903\n"
		<< "To what sports conference do we belong?\t\t" << "Independant (Football)\n"
		<< "When did BYU win the national football title?\t" << "1984\n"
		<< "Who won the Heissman Trophy in 1990?\t\t" << "Ty Detmer\n";
	return 0;
}
