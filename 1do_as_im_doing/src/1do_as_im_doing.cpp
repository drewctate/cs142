//============================================================================
// Name        : 1do_as_im_doing.cpp
// Author      : Andrew
// Version     : 1.0
// Copyright   : Andrew Tate 2015
// Description : BYU Trivia Program
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "What was the original name of BYU?\t\t" << "Brigham Young Acadamy\n"
		<< "When was BYA established?\t\t\t" << "1875\n"
		<< "Who was the first \"permanent\" principle of BYA?\t" << "Karl Maeser\n"
		<< "When did BYA become BYU?\t\t\t" << "1903\n"
		<< "To what sports conference do we belong?\t\t" << "Independant (Football)\n"
		<< "When did BYU win the national football title?\t" << "1984\n"
		<< "Who won the Heissman Trophy in 1990?\t\t" << "Ty Detmer\n";
	return 0;
}
