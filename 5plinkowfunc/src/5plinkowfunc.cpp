/*
 * 4plinko.cpp
 *
 *  Created on: Sep 28, 2015
 *      Author: drewctate
 */#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
using namespace std;

const double slot0 = 100;
const double slot1 = 500;
const double slot2 = 1000;
const double slot3 = 0;
const double slot4 = 10000;
const double slot5 = 0;
const double slot6 = 1000;
const double slot7 = 500;
const double slot8 = 100;
const int numRows = 12;
const double plinkoInc = 0.5;
const int numSlots = 9;


double computeMoneyEarned(double position) {
	double moneyEarned;
	switch(static_cast<int>(position))
	{
		case 0: moneyEarned = slot0; break;
		case 1: moneyEarned = slot1; break;
		case 2: moneyEarned = slot2; break;
		case 3: moneyEarned = slot3; break;
		case 4: moneyEarned = slot4; break;
		case 5: moneyEarned = slot5; break;
		case 6: moneyEarned = slot6; break;
		case 7: moneyEarned = slot7; break;
		case 8: moneyEarned = slot8; break;
	}
	return moneyEarned;
}

double onePeg(double position) {
	int direction;
	direction = rand() % 2;
	if (direction == 1) // if right
	{
		if (position < 8) // if not up against wall
		{
			position += plinkoInc;
		}
		else
		{
			position -= plinkoInc;
		}
	}
	else // if left
	{
		if (position > 0) // if not against wall
		{
			position -= plinkoInc;
		}
		else
		{
			position += plinkoInc;
		}
	}
	return position;
}

void oneChip(int slot) {
	double position = slot;
	cout << "PATH: [";
	for (int i=0; i<numRows; i++) {
		position = onePeg(position);
		if (i == (numRows - 1)) // if last iteration
		{
			cout << fixed << setprecision(1) << position;
		}
		else
		{
			cout << fixed << setprecision(1) << position << " ";
		}
	}
	cout << "]\n";
	double moneyEarned = computeMoneyEarned(position);
	cout << "You earned $" << fixed << setprecision(2) << moneyEarned << endl;
}

void multiChips(int numChips, double slot) {
	double position;
	double moneyEarned = 0;
	for (int n=0; n<numChips; n++) {
		position = slot;
		for (int i=0; i<numRows; i++) {
			position = onePeg(position);
		}
		// compute money earned
		moneyEarned += computeMoneyEarned(position);
	}
	cout << "You earned $" << fixed << setprecision(2) << moneyEarned << endl;
	cout << "Your average was $" << fixed << setprecision(2) <<  moneyEarned / numChips << endl;
	moneyEarned = 0;
}

int main() {
	int operation = 0;
	int slot;
	int numChips;

	srand(time(0));

	while (operation != 4) {
		cout << "MENU\n" <<
				"1 - Drop a single chip into one slot\n" <<
				"2 - Drop multiple chips into one slot\n" <<
				"3 - Drop multiple chips into all slots\n" <<
				"4 - Quit the program\n\n" <<
				"Choose: ";
		if (cin >> operation){
			switch(operation)
			{
				case 1:
					cout << "*** DROP SINGLE CHIP ***\n";
					cout << "Select a slot (0-8): ";
					cin >> slot;
					if (cin.fail()) {
						cin.clear();
						cin.ignore(1000, '\n');
						break;
					}
					if (!((slot >= 0) && (slot < 9))) {
						cout << "INVALID SLOT\n";
						break;
					}
					else {
						oneChip(slot);
					}
					break;

				case 2:
					cout << "*** DROP MULTIPLE CHIPS ***\n";
					cout << "Input number of chips: ";
					cin >> numChips;
					if (cin.fail()) {
						cin.clear();
						cin.ignore(1000, '\n');
						break;
					}
					if (numChips <= 0) {
						cout << "REQUIRES POSITIVE VALUE\n"; // check for negative number chips
						break;
					}

					cout << "Select a slot (0-8): ";
					cin >> slot;
					if (cin.fail()) {
						cin.clear();
						cin.ignore(1000, '\n');
						break;
					}
					if (!((slot >= 0) && (slot < 9))) { // check for invalid slot
						cout << "INVALID SLOT\n";
						break;
					}
					multiChips(numChips, slot);
					break;

				case 3:
					cout << "Enter number of chips: ";
					cin >> numChips;
					if (cin.fail()) {
						cin.clear();
						cin.ignore(1000, '\n');
						break;
					}
					if (numChips <= 0) {
						cout << "REQUIRES POSITIVE VALUE\n"; // check for negative number chips
						break;
					}

					for (int i=0; i<numSlots; i++) {
						cout << "Slot " << i << ":\n";
						multiChips(numChips, i);
						cout << "\n";
					}
					break;

				case 4:
					cout << "*** GOODBYE ***";
					operation = 4;
					break;

				default:
					cout << "INVALID OPTION. Please enter 1, 2, 3 or 4\n";
					break;
			}
		} else {
			cin.clear();
			cin.ignore(1000, '\n');
		}
	}

	return 0;
}
